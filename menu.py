# -*- coding: utf-8 -*-
import re
import urllib.request
from random import *
from bs4 import BeautifulSoup
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter


 
SLACK_TOKEN = "xoxb-688880529351-689741885621-uJZSi7oXXvlbZFqE8OhKtSJQ"
SLACK_SIGNING_SECRET = "333757a2b2063ff9a86c9c7c59d08f2e"

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 크롤링 함수 구현하기
def _crawl_naver_keywords(text, user):
    # 여기에 함수를 구현해봅시다.
    url =  "http://welfoodstory.azurewebsites.net/?category=2%EC%BA%A0%ED%8D%BC%EC%8A%A4-3"
    #url_match = re.search(r'<(http.*?)(\|.*?)?>', url)
    #if not url_match:
    #    return '올바른 URL을 입력해주세요.'

    #url = url_match.group(1)
    sourcecode = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(sourcecode, "html.parser")
    keywords = []
    keywords.append("*<@" + user + ">님 안녕하세요.*\n")
   
    for menu in soup.find_all("span", class_="menu-item-contents-mainfood") :
        print(menu)
        if not menu.get_text() in keywords :
            if len(keywords) >= 10 :
                break
        keywords.append(menu.get_text())
    
    # 키워드 리스트를 문자열로 만듭니다.
    return '\n'.join(keywords)


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    user = event_data["event"]["user"]
   
    if "점심" in text :
        keywords = _crawl_naver_keywords(text, user)
        slack_web_client.chat_postMessage(
            channel=channel,
            text=keywords
        )
    #print(event_data)
    elif "사이트" in text :
        slack_web_client.chat_postMessage(
        channel=channel,
        text="<https://ssafy.elice.io|엘리스>는 정말 최고야!"
        )

    else :   
        slack_web_client.chat_postMessage(
            channel = channel,
            text = "조용히 하세요!"
        )
        slack_web_client.chat_postEphemeral(
            channel = channel,
            user = user,
            text = "조용"
        )
    
   


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"
 
if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)