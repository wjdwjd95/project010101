# -*- coding: utf-8 -*-
import re
import urllib.request
from random import *
from bs4 import BeautifulSoup
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter


 
SLACK_TOKEN = "xoxb-688880529351-689741885621-uJZSi7oXXvlbZFqE8OhKtSJQ"
SLACK_SIGNING_SECRET = "333757a2b2063ff9a86c9c7c59d08f2e"

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

refuse1 = ImageBlock(
    image_url = "http://blogfiles.naver.net/20141026_188/caccat_1414253367193bHXOD_JPEG/IMG_20141009_063520.jpg",
    alt_text = "ERROR IMAGE"
)
refuse2 = ImageBlock(
    image_url = "http://cafefiles.naver.net/20141020_36/wkdgofus123_1413792230507kvBlH_JPEG/1412344745043.jpeg",
    alt_text = "ERROR IMAGE"
)
refuse3 = ImageBlock(
    image_url = "http://blogfiles.naver.net/MjAxNzA1MTRfMjY2/MDAxNDk0NzU1ODk3MDk4.sY_qAIx458dX5GdbFC7COFbcGgnjx8X1CDMxQj080twg.GkNoWmK2gD9irBkN0cdud23XLUEukw3bElQj17otATkg.JPEG.kis6539/JTBC_%BE%C6%B4%C2_%C7%FC%B4%D4.E75.170513.720p-NEXT_0001490664ms.jpg",
    alt_text = "ERROR IMAGE"
)
my_blocks = [refuse1,refuse2,refuse3]

refuse_list = ["싫어", "싫은데요", "굳이", "내가?"]
# 크롤링 함수 구현하기
def _crawl_naver_keywords(text, user):
    # 여기에 함수를 구현해봅시다.
    
    url_match = re.search(r'<(http.*?)(\|.*?)?>', text)
    if not url_match:
        return '올바른 URL을 입력해주세요.'

    url = url_match.group(1)
    sourcecode = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(sourcecode, "html.parser")
    keywords = []
    keywords.append("*<@" + user + ">님 안녕하세요.*\n")
    if "naver" in url :
        for naver_text in soup.find_all("span", class_="ah_k") :
            if not naver_text.get_text() in keywords :
                if len(keywords) >= 10 :
                    break
                keywords.append(naver_text.get_text())
    elif "daum" in url :
        for daum_text in soup.find_all("a", class_="link_issue") :
            if not daum_text.get_text() in keywords :
                if len(keywords) >= 10 :
                    break
                keywords.append(daum_text.get_text())
    # 키워드 리스트를 문자열로 만듭니다.
    return '\n'.join(keywords)


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    user = event_data["event"]["user"]
   
    if "www" in text :
        keywords = _crawl_naver_keywords(text, user)
        slack_web_client.chat_postMessage(
            channel=channel,
            text=keywords
        )
    #print(event_data)
    elif "사이트" in text :
        slack_web_client.chat_postMessage(
        channel=channel,
        text="<https://ssafy.elice.io|엘리스>는 정말 최고야!"
        )
    elif "블록" in text :
        slack_web_client.chat_postMessage(
        channel=channel,
        blocks = extract_json(my_blocks)
        )
    elif "배고파" in text :
        i = randint(0,2)
       
        print(my_blocks[i])
        tmpblock = [my_blocks[i]]
        
        print(tmp)
        slack_web_client.chat_postMessage(
            channel = channel,
            blocks =  extract_json(tmpblock)
        )
    elif "파일" in text :
         file_name = "i16408154752.jpg"
         result = slack_web_client.files_upload(channels = channel,file = file_name)
         if not result["ok"]:
            print("result : " + result["error"])
            slack_web_client.chat_postEphemeral(
            channel = channel,
            user = user,
            text = "result : " + result["error"]
            ) 
    else :   
        slack_web_client.chat_postMessage(
            channel = channel,
            text = "조용히 하세요!"
        )
        slack_web_client.chat_postEphemeral(
            channel = channel,
            user = user,
            text = "조용"
        )
   
           
   


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"
 
if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)