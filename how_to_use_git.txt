0. git이란 ?
소스 코드의 버전을 관리하는 소프트웨어.

1. 오픈 소스 소프트웨어란 ?
라이선스 : 상표 등록된 재산권을 가지고 있는 개인 또는 단체가 타인에게 대가를 받고 그 재산권을 사용할 수 있도록 상업적 권리를 부여하는 계약.

2. Git을 많이 사용하는 첫번째 요인

3. Git을 많이 사용하는 두번째 요인
스냅샷 : 임시 자료를 다루는 수단으로서 특정 시간에 자료의 백업을 수행하기 위한 것.

6. Git 세 가지 영역
작업 디렉토리 -> 준비 영역 -> .git 디렉토리

----------------------------------------------------------------------------

0. 설치 확인 : 
git --version

1. 사용자 정보 설정 : 저장소에 코드를 반영할 때 등록될 사용자 정보를 설정
git config --global user.name "Hahn haeyong"
git config --global user.email "yong80211@gmail.com"

2. 설정 정보 확인
git config --list

3. 기존 디렉토리 사용
git init

4. 디렉토리 저장소 생성 확인
ls -al

5. 저장소 복사(Clone) 사용
git clone

6. 작업 디렉토리에서 준비 영역으로 보내기
git add 파일 이름
git add . 한번에 준비 영역으로 보낼 파일이 많다면 사용

7. 준비 영역에 들어간 걸 다시 취소하고 싶다면
git reset 준비 영역에 들어간 파일 이름

8. 현재 진행 상태 확인
git status

9. 준비 영역에 있는 파일들을 저장소에 반영 
git commit -m "Initial commit" 

10. 저장소 반영 내용 변경
git commit --amend -> 편집기로 넘어감

11. 저장소에 반영되 있는 걸 삭제하고 싶으면
git reset --hard

12. 저장소 반영 내역이 궁긍하다면(commit된 내역이 궁금하다면)
git log

----------------------------------------------------------------------------

0. Git 가지 치기
예를 들어, 3명이서 맨 처음에 하나의 기존 소스 코드를 가지고 협업을 시작한다. 그 기존 소스 코드를 3개로 만든다(가지 치기 3개(=git branch) 3개 생성)
그리고나서, 각자 진행한 소스 코드를 맨 처음에 소스 코드랑 합친다(git merge)
=> 이런 식으로 진행하면 좋은 이유 ?
1. 버전 관리가 잘됨
2. 에러가 발생했을 때, 어떤 사람이 에러를 발생시켰는지 찾을 수 있다.

--------------------------------------------------------------------------------

0. git branch 생성
git branch 만들 branch 이름

1. git branch 확인
git branch

2. git branch 전환
git checkout 전환할 branch 이름
=> 여기서 git checkout -b 만들 branch 이름 을 사용하면 생성과 동시에 
전환이 가능하다.
=> git checkout 후 작업된 내역은 모두 해당 branch에 기록된다. master branch에 기록되는 게 아니라!  
=> 나중에 merge하게 되면 그 때 master branch에 적용됨!

3. merge하는 방법
먼저 branch에서 작업 중이였지만 master branch로 일단 돌아온다. (git checkout master)
그 다음, git merge 합칠 브랜치 이름으로 merge한다.

4. master branch에 merge된 branch들 보기
git branch --merged

5. master branch에 merge되어 필요 없어진 branch들 삭제
git branch -d 삭제하고자하는 branch이름

----------------------------------------------------------------------------------------------------------------------------

0. 원격 저장소 : github, gitlab

1. 연결된 원격 저장소 확인
git remote

2. 원격 저장소 추가
git remote add "원격저장소이름지정" "원격 저장소 주소"

3. 원격 저장소 살펴보기
git remote show "원격저장소이름지정한 거 적으면 됨"

4. 원격 저장소 이름 변경
git remote rename "원래 원격저장소이름" "바꿀원격저장소이름"

5. 필요업서진 원격 저장소 삭제
git remote rm "우리가 삭제할 원격 저장소 이름"

6. 원격 저장소에 우리 로컬 .git 저장소에 있는거 넣기
git push (git pull하기 전에, git add하고 git commit까지한 것만 git pull 됨) 
git push -u origin master
7. 원격 저장소에 있는 거 가져와서 우리 로컬 .git 저장소에 있는 거랑 
같은 파일 있으면 병합까지
git pull

8. 원격 저장소에 있는 거 가져오는 것만
git fetch
=> 병합까지 할려면 
git log로 변경된 파일 확인하고 git merge 원격저장소/master 하면 됨

